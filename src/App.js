import './App.css';
import React, { Component } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css'

import reducers from './reducers'

import Chat from './components/Chat/Chat';

const store = createStore(reducers);

class App extends Component {

  render() {
    return (
      <Provider store={store} >
        <Router>
          <Route path='/:id' component={Chat} />
        </Router>
      </Provider>);
  }
}

export default App;
