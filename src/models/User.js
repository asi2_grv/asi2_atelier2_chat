/**
 * Format string with name format (1 uppercase, all other lowercase) -> doesn't work with names including "-"
 * @param {*} s string to format
 */
function formatName(s) {
    return s.slice(0, 1).toUpperCase() + s.slice(1).toLowerCase();
}
/**
 * User model
 */
class User {
    constructor({ id, surName, lastName }) {
        this.id = Number(id);
        this.surName = formatName(surName);
        this.lastName = formatName(lastName);
    }
}
export default User;