import { Component } from 'react';
import { connect } from 'react-redux';

import { Segment } from 'semantic-ui-react';

import History from './components/History';
import UserSelect from './components/UserSelect';
import Input from './components/Input';
import ChatUserInfo from './components/ChatUserInfo';
import UserModel from '../../models/User';

import { loadUsers, selectCurrentUser, selectMessageUser } from '../../actions';

import chatSocket from '../../network/ChatSocket';
import chatREST from '../../network/ChatREST';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.onGetUserList = this.onGetUserList.bind(this);

        //TODO: instead of getting all users from Spring, get connected user ids from GET {node}/chat/users
        // We can't do it now as we need to know the current user ID to get it to work, and we use the list to do that
        // It should be done with auth in production
        chatREST.getUsers(this.onGetUserList);        
    }
    onGetUserList(err, data) {
        let availableUsers = [];
        console.log(data)
        for (const user of data) {
            availableUsers.push(new UserModel(user));
        }
        const currentUser = availableUsers[this.props.match.params.id ? this.props.match.params.id : 0];
        availableUsers = availableUsers.filter(user => user.id !== currentUser.id);



        this.props.dispatch(loadUsers(availableUsers));
        this.props.dispatch(selectMessageUser(availableUsers[0]));
        this.props.dispatch(selectCurrentUser(currentUser));

        chatSocket.setSenderId(currentUser.id);
    }
    render() {
        return (
            <Segment style={{ overflow: 'auto', maxWidth: "25rem" }}>
                <ChatUserInfo />
                <UserSelect />
                <History />
                <Input />
            </Segment>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentUser: state.chatReducer.currentUser
    }
};

export default connect(mapStateToProps)(Chat);
