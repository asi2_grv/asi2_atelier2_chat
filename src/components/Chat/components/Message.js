import { Component } from 'react';
import { connect } from 'react-redux';

import { Segment, Label } from 'semantic-ui-react';

class Message extends Component {
    constructor(props) {
        super(props);
        this.getUserName = this.getUserName.bind(this);
    }
    formatTime(time) {
        const date = new Date(time * 1000);//Unix timestamp to Date
        const H = date.getHours().toString().padStart(2, '0');
        const m = date.getMinutes().toString().padStart(2, '0');
        const s = date.getSeconds().toString().padStart(2, '0');
        return `${H}:${m}:${s}`;
    }
    getUserName(id) {
        return this.props.userList.find(user => user.id === id)?.surName;
    }
    render() {
        const color = this.props.sender === this.props.currentUser.id ? "green" : "blue";
        const ribbonPos = this.props.sender === this.props.currentUser.id ? "right" : true;
        const name = this.props.sender === this.props.currentUser.id ? "Me" : this.getUserName(this.props.sender);
        const dateString = this.formatTime(this.props.time);
        return (
            <Segment raised>
                <Label as="a" color={color} ribbon={ribbonPos}>{name}</Label>
                <span> {dateString}</span>
                <p>{this.props.content}</p>{/* TODO filter language ?*/}
            </Segment>);
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentUser: state.chatReducer.currentUser,
        userList: state.chatReducer.userList
    }
};

export default connect(mapStateToProps)(Message);
