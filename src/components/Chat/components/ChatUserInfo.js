import { Component } from 'react';
import { connect } from 'react-redux';

import { Segment, Label, Grid, Icon } from 'semantic-ui-react';

class ChatUserInfo extends Component {
    render() {
        return (
            <Segment>
                <Label attached="top">
                    <Grid columns={2}>
                        <Grid.Column>Chat</Grid.Column>
                        <Grid.Column>
                            <Grid columns={2}>
                                <Grid.Column>{`${this.props.currentUser?.surName} ${this.props.currentUser?.lastName}`}</Grid.Column>
                                <Grid.Column>
                                    <Icon name="user circle"></Icon>
                                </Grid.Column>
                            </Grid>
                        </Grid.Column>
                    </Grid>
                </Label>
            </Segment>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        currentUser: state.chatReducer.currentUser
    }
};
export default connect(mapStateToProps)(ChatUserInfo);
