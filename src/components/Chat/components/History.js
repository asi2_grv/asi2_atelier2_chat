import { Component } from 'react';
import { connect } from 'react-redux';

import { Segment } from 'semantic-ui-react';

import { animateScroll } from 'react-scroll';

import Message from './Message'

import { addMessage } from '../../../actions';

import chatSocket from '../../../network/ChatSocket';

class History extends Component {
    constructor(props) {
        super(props);

        this.onMessage = this.onMessage.bind(this);

        chatSocket.subscribeToMessage(this.onMessage);
    }
    onMessage(err, data) {
        if (this.props.currentMessageUser.id === data.sender || this.props.currentMessageUser.id === data.receiver)
            this.props.dispatch(addMessage(data));
        else {
            //TODO: handle notification (we receive some messages that are not displayed, we could use that as a notification system)
        }
    }
    componentDidUpdate(prevProps) {
        if (prevProps.messageList?.length !== this.props.messageList.length)
            this.scrollToBottom();
    }
    scrollToBottom() {
        animateScroll.scrollToBottom({
            containerId: "chat_autoscroll",
            duration: 200
        });
    }
    render() {
        const messages = [];
        for (const idx in this.props.messageList) {
            const msg = this.props.messageList[idx];
            messages.push(
                <Message
                    key={idx}
                    sender={msg.sender}
                    time={msg.time}//TODO: format ?
                    content={msg.content}
                />
            )
        }
        return (
            <Segment id='chat_autoscroll' style={{ overflow: 'auto', height: "20rem" }}>
                {messages}
            </Segment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        messageList: state.chatReducer.messageList,
        currentMessageUser: state.chatReducer.currentMessageUser
    }
};

export default connect(mapStateToProps)(History);
