import { Component } from 'react';
import { connect } from 'react-redux';

import { Form, Button, Icon, TextArea } from 'semantic-ui-react';

import { addMessage } from '../../../actions';

import chatSocket from '../../../network/ChatSocket';

class Input extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.handleInput = this.handleInput.bind(this);

        this.content = "";
    }

    handleInput(ev, { value: content }) {
        this.content = content;
    }
    handleOnClick() {
        chatSocket.sendMessage({
            sender: this.props.currentUser.id,
            receiver: this.props.currentMessageUser.id,
            content: this.content
        })

        this.props.dispatch(addMessage({
            sender: this.props.currentUser.id,
            time: Math.floor(+new Date() / 1000),
            content: this.content
        }));
    }


    render() {
        return (
            <Form>
                <Form.Field>
                    <TextArea rows={2} style={{ resize: "None" }} onChange={this.handleInput}></TextArea>
                </Form.Field>
                <Button fluid icon labelPosition="right" onClick={this.handleOnClick}>
                    <Icon name="right arrow" />
                Send
            </Button>
            </Form>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentMessageUser: state.chatReducer.currentMessageUser,
        currentUser: state.chatReducer.currentUser
    }
};

export default connect(mapStateToProps)(Input);
