class REST {
    fetchWithCb(cb, url) {
        fetch(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((res) => { return res.json() })
            .then((data) => cb(null, data))
            .catch((err) => cb(err));
    }
    fetchWithCbAndParams(cb, url, params) {
        url = new URL(url);
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
        this.fetchWithCb(cb, url);
    }
}
module.exports = REST;