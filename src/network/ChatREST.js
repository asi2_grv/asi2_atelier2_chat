const REST = require("./REST");
class ChatREST extends REST {
    constructor() {
        super();
        this.baseUrl = "http://localhost:8082";
    }
    getUsers(cb) {
        //TODO: don't get all users, get limit 10 (max dropdown), depending on search content + update on each search
        // OR: get only online user ids (from node back using rooms) + get their data 1 by 1 from spring back
        const url = this.baseUrl + "/users";
        this.fetchWithCb(cb, url);
    }
    getHistory(cb, { sender, receiver }) {
        const url = this.baseUrl + "/chatMessages";
        this.fetchWithCbAndParams(cb, url, { sender, receiver })
    }
}

export default new ChatREST();