export const loadMessages = (messageList) => {
    return { type: 'LOAD_MESSAGES', messageList };
}

export const addMessage = (message) => {
    return { type: 'ADD_MESSAGE', message };
}

export const loadUsers = (userList) => {
    return { type: 'LOAD_USERS', userList };
}
export const selectCurrentUser = (currentUser) => {
    return { type: 'SELECT_CURRENT_USER', currentUser };
}

export const selectMessageUser = (currentMessageUser) => {
    return { type: 'SELECT_MESSAGE_USER', currentMessageUser };
}