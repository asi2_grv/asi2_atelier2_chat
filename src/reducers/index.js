import { combineReducers } from 'redux';
import chatReducer from './chatReducer';

const globalReducer = combineReducers({
    chatReducer
});
export default globalReducer;