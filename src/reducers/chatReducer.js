const chatReducer = (state = { messageList: [] }, action) => {
    const newState = { ...state };
    switch (action.type) {
        case 'LOAD_MESSAGES':
            newState.messageList = action.messageList;
            break;
        case 'ADD_MESSAGE':
            const newMessageList = [...newState.messageList];
            newMessageList.push(action.message);
            newState.messageList = newMessageList;
            break;
        case 'LOAD_USERS':
            newState.userList = action.userList;
            break;
        case 'SELECT_CURRENT_USER':
            newState.currentUser = action.currentUser;
            break;
        case 'SELECT_MESSAGE_USER':
            newState.currentMessageUser = action.currentMessageUser;
            break;
        default:
            break;
    }
    return newState;
}

export default chatReducer;